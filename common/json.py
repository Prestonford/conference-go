from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, object):
        if isinstance(object, self.model):
            dict = {}
            if hasattr(object, "get_api_url"):
                dict["href"] = object.get_api_url()
            for property in self.properties:
                value = getattr(object, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                dict[property] = value
            dict.update(self.get_extra_data(object))
            return dict
        else:
            return super().default(object)

    def get_extra_data(self, object):
        return {}
