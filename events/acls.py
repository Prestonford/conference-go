import requests
from .keys import PEXELS_API_KEY, WEATHER_DATA_KEY
import json


def get_city_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    params = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    response = requests.get(
        "https://api.pexels.com/v1/search", params=params, headers=headers
    )
    photo_dict = json.loads(response.content)
    try:
        return photo_dict["pictures"][0]["url"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    query = f"{city}, {state}, US"
    print(f"{city}, {state}, US")
    api_key = WEATHER_DATA_KEY
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={query}&appid={api_key}"
    geocode_response = requests.get(geocode_url)
    geocode_data = geocode_response.json()
    print(geocode_data)
    lat = geocode_data[0]["lat"]
    lon = geocode_data[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()

    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    weather_dict = {"temp": temp, "description": description}
    return weather_dict
